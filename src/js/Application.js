import EventEmitter from "eventemitter3";
import image from "../images/planet.svg";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();

    const loading = document.getElementsByTagName("progress")[0];
    this._loading = loading;
    this._stopLoading();

    this._newPlanets = [];
    this._nextRequest = "https://swapi.boom.dev/api/planets";
   
    this._load();
    this.emit(Application.events.READY);
  }

  _render({ name, terrain, population }) {
    return `
<article class="media">
  <div class="media-left">
    <figure class="image is-64x64">
      <img src="${image}" alt="planet">
    </figure>
  </div>
  <div class="media-content">
    <div class="content">
    <h4>${name}</h4>
      <p>
        <span class="tag">${terrain}</span> <span class="tag">${population}</span>
        <br>
      </p>
    </div>
  </div>
</article>
    `;
  }

  async _load() { 
    this._startLoading();
    try {
      let result = await fetch(this._nextRequest);

      if (!result.ok) {
        throw "Error occurred while fetching planets!";
      }

      let json = await result.json();
      let planets = json.results;
      let nextRequest = json.next;

      this._newPlanets = planets;
      this._create();

      if (nextRequest) {
        this._nextRequest = nextRequest;
        this._load();
      }
    } catch (error) {
      console.log(`Error: ${error}`);
    } finally {
      this._stopLoading();
    }
  }

  _create() { 
    this._newPlanets.forEach(planet => {
      const box = document.createElement("div");
      box.classList.add("box");
      box.innerHTML = this._render({
        name: planet.name,
        terrain: planet.terrain,
        population: planet.population,
     });

      document.body.querySelector(".main").appendChild(box);
    });
    
  }

  _startLoading() { 
    this._loading.style.display = "block";
  }

  _stopLoading() { 
    this._loading.style.display = "none";
  }
}
